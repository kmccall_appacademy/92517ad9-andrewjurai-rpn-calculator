class RPNCalculator
  attr_accessor :calculator

  def initialize
    @calculator = Array.new
  end

  def push(int)
    @calculator << int
  end

  def value
    @calculator.last
  end

  def update_set_with(results)
    remove_used_operands!(@calculator)
    @calculator << results
  end

  def remove_used_operands!(array)
    array.pop(2)
  end

  def any_errors?
    raise "calculator is empty" if @calculator.empty?
  end

  def evaluate(string)
    ops_seq = tokens(string)
    ops_seq.length.times do
      ref_char = ops_seq.shift
      if operator?(ref_char)
        operate_with(ref_char)
      else @calculator << ref_char
      end
    end
    @calculator.last
  end

  def tokens(str)
    str.split.map do |el|
      if "+-!/**^".include? el
        el.to_sym
      else el.to_i
      end
    end
  end

  def operator?(el)
    [:*,:+,:-,:/].include?(el)
  end

  def operate_with(ref_char)

    if ref_char == :+
      plus
    elsif ref_char == :-
      minus
    elsif ref_char == :/
      divide
    elsif ref_char == :*
      times
    end
  end

  def plus
    any_errors?
    values_added = @calculator[-2..-1].reduce(:+)
    update_set_with(values_added)
  end

  def minus
    any_errors?
    values_subtracted = @calculator[-2..-1].reduce(:-)
    update_set_with(values_subtracted)
  end

  def divide
    any_errors?
    dividend = @calculator[-2..-1].first.to_f
    divisor = @calculator[-2..-1].last.to_f
    quotient = dividend / divisor
    update_set_with(quotient)
  end

  def times
    any_errors?
    product = @calculator[-2..-1].reduce(:*)
    update_set_with(product)
  end

end
